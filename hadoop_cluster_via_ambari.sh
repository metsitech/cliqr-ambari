#!/bin/bash

cliqr_userenv_file='/usr/local/osmosix/etc/userenv'

echo "INFO: Setting environment variables from: '${cliqr_userenv_file}'."
. $cliqr_userenv_file

echo "Setting ambari manager fqdn from $cliqrAppTierName (this curl will happen locally)"
ambari_manager_fqdn=$cliqrAppTierName

cluster_name="cluster_$ambari_manager_fqdn"
hostmapping_json_file="/tmp/${cluster_name}-hostmapping.json"
configuration_json_file="/tmp/${cluster_name}-configuration.json"
blueprint_name="blueprint-${cluster_name}"

echo "Forming amabari hosts from $CliqrDependents"
CliqrDependenciesList=(${CliqrDependencies//,/ })
# Open the array:
ambari_hosts_json=""

# Build the array of json line by line (in a custom format of params just for CliQr - { "fqdn" : "<fqdn-of-single-node-cluster-machine>" } , { .. } ):
for dep in $CliqrDependenciesList
do
  ambari_hosts_json="$ambari_hosts{\"fqdn\" : \"$dep\"},"  
done

# Close the array by replacing the last comma (no more elements):
ambari_hosts=$( sed 's/\(.*\)},/\1}/' <<< $ambari_hosts)

echo "Ambari hosts: $ambari_hosts"

echo "Building configuration content in: $configuration_json_file"
read -d '' json_configuration_content << EOF
{
  "configurations" : [ ],
  "host_groups" : [
    {
      "name" : "host_group_1",
      "components" : [
        {
          "name" : "NAMENODE"
        },
        {
          "name" : "SECONDARY_NAMENODE"
        },
        {
          "name" : "DATANODE"
        },
        {
          "name" : "HDFS_CLIENT"
        },
        {
          "name" : "RESOURCEMANAGER"
        },
        {
          "name" : "NODEMANAGER"
        },
        {
          "name" : "YARN_CLIENT"
        },
        {
          "name" : "HISTORYSERVER"
        },
        {
          "name" : "APP_TIMELINE_SERVER"
        },
        {
          "name" : "MAPREDUCE2_CLIENT"
        },
        {
          "name" : "ZOOKEEPER_SERVER"
        },
        {
          "name" : "ZOOKEEPER_CLIENT"
        }
      ],
      "cardinality" : "1"
    }
  ],
  "Blueprints" : {
    "blueprint_name" : "single-node-hdp-cluster",
    "stack_name" : "HDP",
    "stack_version" : "2.3"
  }
}
EOF

echo -en $json_configuration_content > $configuration_json_file

echo "Submitting request to register the blueprint with: $configuration_json_file"
curl -H "X-Requested-By: ambari" -X POST -u admin:admin "http://${ambari_manager_fqdn}:8080/api/v1/blueprints/${blueprint_name}" -d @$configuration_json_file



echo "Building hostmapping content in: $hostmapping_json_file"
read -d '' json_configuration_content << EOF
{
  "blueprint" : "single-node-hdp-cluster",
  "default_password" : "admin",
  "host_groups" :[
    {
      "name" : "host_group_1",
      "hosts" : [
        {
          $ambari_hosts
        }
      ]
    }
  ]
}
EOF
echo -en $json_hostmapping_content > $hostmapping_json_file

echo "Submitting request to start the cluster installation with: $hostmapping_json_file"
curl -H "X-Requested-By: ambari" -X POST -u admin:admin "http://${ambari_manager_fqdn}:8080/api/v1/clusters/${cluster_name}" -d @$hostmapping_json_file